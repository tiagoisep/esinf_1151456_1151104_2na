/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listas;

import esinfp1.Reparticao;
import esinfp1.Servico;
import java.util.ArrayList;

/**
 *
 * @author Carlos
 */
public class ListaServicos {
    private ArrayList<Servico> listaServicos;

    @Override
    public String toString() {
        return "ListaServicos{" + "listaServicos=" + listaServicos + '}';
    }
  
    
    public ListaServicos(){
        this.listaServicos=new ArrayList<>();
         }

    public ArrayList<Servico> getListaServicos() {
        return listaServicos;
    }

    public void setListaServicos(ArrayList<Servico> listaServicos) {
        this.listaServicos = listaServicos;
    }
    
    public void addToListaServicos(Servico s){
        listaServicos.add(s);
    }
    
    public Servico novoServico(){
        return new Servico();
    }

    
}
