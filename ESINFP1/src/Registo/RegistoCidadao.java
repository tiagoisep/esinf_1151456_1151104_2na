/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Registo;

import esinfp1.Cidadao;
import esinfp1.Reparticao;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Carlos
 */
public class RegistoCidadao {
    /**
     * variavel registo do cidadao do tipo list
     */
    private List<Cidadao> registoCidadao;

    /**
     * construtor registocidadao
     */
    public RegistoCidadao() {
        registoCidadao = new ArrayList<>();
    }

    /**
     * construtor registocidadao
     * @param outroRegisto outroreg
     */
    public RegistoCidadao(RegistoCidadao outroRegisto) {
        this.registoCidadao = new ArrayList<>(outroRegisto.getRegistoCidadao());
    }

    public List<Cidadao> getRegistoCidadao() {
        return new ArrayList<>(this.registoCidadao);
    }

    /**
     * regista cidadao
     *
     * @param c cidadao
     * @return boolean
     */
    public boolean registaCidadao(Cidadao c) {
        return this.registoCidadao.add(c);
    }
    
    public List<Cidadao> getListaCidadaoReparticao(List<Cidadao> registoCidadao, Reparticao r){
        List<Cidadao> listaCidadaoRep = new ArrayList<>();
        for(Cidadao cid : registoCidadao){
            if(cid.getNreparticao().equals(r.getNreparticao())){
                listaCidadaoRep.add(cid);
            }
        }
        return listaCidadaoRep;
    }
    /**
     * valida cidadao
     *
     * @param C cidadao
     * @return bollean
     */
    public boolean validaCidadao(Cidadao C) {
        if (registoCidadao.contains(C)) {
            return false;
        }
        return true;
    }

}
