/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Registo;

import esinfp1.Cidadao;
import esinfp1.Reparticao;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 *
 * @author Tiago Ferreira
 */
public class RegistoReparticao<Reparticao> implements Iterable<Reparticao>, Cloneable {

    private final Node<Reparticao> head;
    private final Node<Reparticao> tail;
    private int size;
    private int modCount = 0;

    public RegistoReparticao() {
        head = new Node<>(null, null, null);
        tail = new Node<>(null, head, null);
        head.setNext(tail);
    }

    /**
     * returns the size of the linked list
     *
     * @return
     */
    public int size() {
        return size;
    }

    /**
     * return whether the list is empty or not
     *
     * @return
     */
    public boolean ReparticaoisEmpty() {
        return size == 0;
    }

    public Node novo;

    /**
     * adds element at the end of the linked list
     *
     * @param r
     */
    public void addReparticaoLast(Reparticao r) {
        novo = new Node(r, head, tail);
        novo.setNext(tail);
        novo.setPrev(tail.getPrev());
        head.setNext(novo);
        tail.setPrev(novo);
                  
        }
    

    
    /**
     * this method walks forward through the linked list
     */
    public void iterateForward() {

        System.out.println("iterating forward..");
        Node tmp = head;
        while (tmp != null) {
            System.out.println(tmp.repart);
            tmp = tmp.next;
        }
    }

    /**
     * this method walks backward through the linked list
     */
    public void iterateBackward() {

        System.out.println("iterating backword..");
        Node tmp = tail;
        while (tmp != null) {
            System.out.println(tmp.repart);
            tmp = tmp.prev;
        }
    }

    /**
     * this method removes element from the start of the linked list
     *
     * @return
     */
    public Reparticao removeFirstReparticao() {
        if (size == 0) {
            throw new NoSuchElementException();
        }
        head.getNext().getNext().setPrev(head);
        head.setNext(head.getNext().getNext());

        return head.getNext().getReparticao();
    }

    /**
     * this method removes element from the end of the linked list
     *
     * @return
     */
    public Reparticao removeLastReparticao() {
        if (size == 0) {
            throw new NoSuchElementException();
        }
        tail.getPrev().getPrev().setNext(tail);
        tail.setPrev(tail.getPrev().getPrev());

        return tail.getPrev().getReparticao();
    }

    @Override
    public Iterator<Reparticao> iterator() {
        return new DoublyLinkedListIterator();
    }

    @Override
    public void forEach(Consumer<? super Reparticao> action) {
        Iterable.super.forEach(action); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Spliterator<Reparticao> spliterator() {
        return Iterable.super.spliterator(); //To change body of generated methods, choose Tools | Templates.
    }

    private class DoublyLinkedListIterator implements ListIterator<Reparticao> {

        private RegistoReparticao.Node<Reparticao> nextNode, prevNode, lastReturnedNode; // node that will be returned using next and prev respectively
        private int nextIndex;  // Index of the next element
        private int expectedModCount;  // Expected number of modifications = modCount;

        public DoublyLinkedListIterator() {
            this.prevNode = head;
            this.nextNode = head.getNext();
            lastReturnedNode = null;
            nextIndex = 0;
            expectedModCount = modCount;
        }

        final void checkForComodification() {  // invalidate iterator on list modification outside the iterator
            if (modCount != expectedModCount) {
                throw new ConcurrentModificationException();
            }
        }

        @Override
        public boolean hasNext() {
            return nextNode != tail;
        }

        @Override
        public Reparticao next() throws NoSuchElementException {
            checkForComodification();

            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean hasPrevious() {
            return prevNode != head;
        }

        @Override
        public Reparticao previous() throws NoSuchElementException {
            checkForComodification();

            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public int nextIndex() {
            return nextIndex;
        }

        @Override
        public int previousIndex() {
            return nextIndex - 1;
        }

        @Override
        public void remove() throws NoSuchElementException {
            checkForComodification();

            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void set(Reparticao r) throws NoSuchElementException {
            if (lastReturnedNode == null) {
                throw new NoSuchElementException();
            }
            checkForComodification();

            lastReturnedNode.setElement(r);
        }

        @Override
        public void add(Reparticao r) {
            checkForComodification();

            throw new UnsupportedOperationException("Not supported yet.");
        }

    }

    public static class Node<Reparticao> {

        // reference to the element stored at this node
        private Node<Reparticao> prev;   // reference to the previous node in the list
        private Node<Reparticao> next;  // reference to the subsequent node in the list
        private Reparticao repart;

        public Node(Reparticao r, Node<Reparticao> next, Node<Reparticao> prev) {
            this.repart = r;
            this.next = next;
            this.prev = prev;
        }

        public Reparticao getReparticao() {
            return repart;
        }

        public Node<Reparticao> getPrev() {
            return prev;
        }

        public Node<Reparticao> getNext() {
            return next;
        }

        public void setElement(Reparticao repart) { // Not on the original interface. Added due to list iterator implementation
            this.repart = repart;
        }

        public void setPrev(Node<Reparticao> prev) {
            this.prev = prev;
        }

        public void setNext(Node<Reparticao> next) {
            this.next = next;
        }
    }

}
