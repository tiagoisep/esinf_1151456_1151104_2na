/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinfp1;

import java.util.Objects;

/**
 *
 * @author Tiago Ferreira
 */
public class Cidadao {

    private String Ncontribuinte;
    private String Nome;
    private String EnderecoEmail;
    private String PostCod;
    private String Nreparticao;

    private static final String N_CONTRIBUINTE_OMISSAO = "Sem Dados";
    private static final String NOME_OMISSAO = "Sem Dados";
    private static final String EMAIL_OMISSAO = "Sem Dados";
    private static final String POST_CODE_OMISSAO = "Sem Dados";
    private static final String N_REPARTICAO_OMISSAO = "Sem Dados";

    public Cidadao(String Ncontribuinte, String Nome, String EnderecoEmail,
            String PostCod, String Nreparticao) {

        this.Ncontribuinte = Ncontribuinte;
        this.Nome = Nome;
        this.EnderecoEmail = EnderecoEmail;
        this.PostCod = PostCod;
        this.Nreparticao = Nreparticao;

    }

    public Cidadao() {

        this.Ncontribuinte = N_CONTRIBUINTE_OMISSAO;
        this.Nome = NOME_OMISSAO;
        this.EnderecoEmail = EMAIL_OMISSAO;
        this.PostCod = POST_CODE_OMISSAO;
        this.Nreparticao = N_REPARTICAO_OMISSAO;
    }

    /**
     * @return the Ncontribuinte
     */
    public String getNcontribuinte() {
        return Ncontribuinte;
    }

    /**
     * @param Ncontribuinte the Ncontribuinte to set
     */
    public void setNcontribuinte(String Ncontribuinte) {
        this.Ncontribuinte = Ncontribuinte;
    }

    /**
     * @return the Nome
     */
    public String getNome() {
        return Nome;
    }

    /**
     * @param Nome the Nome to set
     */
    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    /**
     * @return the EnderecoEmail
     */
    public String getEnderecoEmail() {
        return EnderecoEmail;
    }

    /**
     * @param EnderecoEmail the EnderecoEmail to set
     */
    public void setEnderecoEmail(String EnderecoEmail) {
        this.EnderecoEmail = EnderecoEmail;
    }

    /**
     * @return the PostCod
     */
    public String getPostCod() {
        return PostCod;
    }
    
    /**
     *
     * @return reduced postCod
     */
    public String getPostCodReduced() {
        return PostCod.substring(0, 4);
    }

    /**
     * @param PostCod the PostCod to set
     */
    public void setPostCod(String PostCod) {
        this.PostCod = PostCod;
    }

    /**
     * @return the Nreparticao
     */
    public String getNreparticao() {
        return Nreparticao;
    }

    /**
     * @param Nreparticao the Nreparticao to set
     */
    public void setNreparticao(String Nreparticao) {
        this.Nreparticao = Nreparticao;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cidadao other = (Cidadao) obj;
        if (!Objects.equals(this.Ncontribuinte, other.Ncontribuinte)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Cidadao{" + "Ncontribuinte=" + Ncontribuinte + ", Nome=" + Nome + ", EnderecoEmail=" + EnderecoEmail + ", PostCod=" + PostCod + ", Nreparticao=" + Nreparticao + '}';
    }

}
