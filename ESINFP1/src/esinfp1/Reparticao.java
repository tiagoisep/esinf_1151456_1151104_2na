/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinfp1;

import Listas.ListaServicos;
import java.util.*;

/**
 *
 * @author Tiago Ferreira
 */
public class Reparticao {

    private String cidade;
    private String Nreparticao;
    private String postCod;
    private List<Servico> listaServicos;
    private List<Cidadao> registoCidadao;
    
    private static final String CIDADE_OMISSAO="Sem Dados";
    private static final String N_REPARTICAO_OMISSAO="Sem Dados";
    private static final String POST_COD_OMISSAO="Sem Dados";
    
    public Reparticao(String cidade, String Nreparticao, String postCod, List<Servico> listaServicos, List<Cidadao> listaCidadao){
        this.cidade=cidade;
        this.Nreparticao=Nreparticao;
        this.postCod=postCod;
        this.listaServicos=new ArrayList<>();
        this.registoCidadao=new ArrayList<>();
        
    }
    
    public Reparticao(List<Servico> listaServicos, List<Cidadao>listaCidadao){
        this.cidade=CIDADE_OMISSAO;
        this.Nreparticao=N_REPARTICAO_OMISSAO;
        this.postCod=POST_COD_OMISSAO;
        this.listaServicos=new ArrayList<>();
        this.registoCidadao=new ArrayList<>();
    }

    public List<Cidadao> getRegistoCidadao() {
        return registoCidadao;
    }

    public void setRegistoCidadao(List<Cidadao> registoCidadao) {
        this.registoCidadao = registoCidadao;
    }


    /**
     * @return the cidade
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * @param cidade the cidade to set
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * @return the Nreparticao
     */
    public String getNreparticao() {
        return Nreparticao;
    }

    /**
     * @param Nreparticao the Nreparticao to set
     */
    public void setNreparticao(String Nreparticao) {
        this.Nreparticao = Nreparticao;
    }

    /**
     * @return the postCod
     */
    public String getPostCod() {
        return postCod;
    }

    /**
     * @param postCod the postCod to set
     */
    public void setPostCod(String postCod) {
        this.postCod = postCod;
    }

    /**
     * @return the listaServicos
     */
    public List<Servico> getListaServicos() {
        return listaServicos;
    }

    /**
     * @param listaServicos the listaServicos to set
     */
    public void setListaServicos(List<Servico> listaServicos) {
        this.listaServicos = listaServicos;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Reparticao other = (Reparticao) obj;
        if (!Objects.equals(this.Nreparticao, other.Nreparticao)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Reparticao{" + "cidade=" + cidade + ", Nreparticao=" + Nreparticao + ", postCod=" + postCod + ", listaServicos=" + listaServicos + ", registoCidadao=" + registoCidadao + '}';
    }
    
}
