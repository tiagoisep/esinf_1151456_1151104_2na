/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinfp1;

import java.util.Objects;

/**
 *
 * @author Tiago Ferreira
 */
public class Senha {
    
    private String Nordem;
    private String CodAssunto;
    private String NContribuinte;
    private static final String N_ORDEM_OMISSAO="Sem Numero de Ordem";
    private static final String COD_ASSUNTO_OMISSAO="Sem Codigo";
    private static final String N_CONTRIB_OMISSAO="Sem contribuinte";
    
    public Senha(String Nordem, String CodAssunto, String NContribuinte){
        this.Nordem=Nordem;
        this.CodAssunto=CodAssunto;
        this.NContribuinte=NContribuinte;
    }
    public Senha(){
        this.Nordem=N_ORDEM_OMISSAO;
        this.CodAssunto=COD_ASSUNTO_OMISSAO;
        this.NContribuinte=N_CONTRIB_OMISSAO;
    }

    /**
     * @return the Nordem
     */
    public String getNordem() {
        return Nordem;
    }

    /**
     * @param Nordem the Nordem to set
     */
    public void setNordem(String Nordem) {
        this.Nordem = Nordem;
    }

    /**
     * @return the CodAssunto
     */
    public String getCodAssunto() {
        return CodAssunto;
    }

    /**
     * @param CodAssunto the CodAssunto to set
     */
    public void setCodAssunto(String CodAssunto) {
        this.CodAssunto = CodAssunto;
    }

    public String getNContribuinte() {
        return NContribuinte;
    }

    public void setNContribuinte(String NContribuinte) {
        this.NContribuinte = NContribuinte;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Senha other = (Senha) obj;
        if (!Objects.equals(this.NContribuinte, other.NContribuinte)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Senha{" + "Nordem=" + Nordem + ", CodAssunto=" + CodAssunto + ", NContribuinte=" + NContribuinte + '}';
    }
    
    
}
