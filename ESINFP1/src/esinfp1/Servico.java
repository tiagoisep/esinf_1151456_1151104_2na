/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinfp1;

import java.util.Objects;

/**
 *
 * @author Carlos
 */
public class Servico {
    private String tipoServico;
    
    public Servico(String tipoServico){
        this.tipoServico=tipoServico;
    }

    public Servico() {
        this.tipoServico="";
    }

    public String getTipoServico() {
        return tipoServico;
    }

    public void setTipoServico(String tipoServico) {
        this.tipoServico = tipoServico;
    }

    @Override
    public String toString() {
        return "Serviço" + tipoServico + ".";
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Servico other = (Servico) obj;
        if (!Objects.equals(this.tipoServico, other.tipoServico)) {
            return false;
        }
        return true;
    }
    
    
}
