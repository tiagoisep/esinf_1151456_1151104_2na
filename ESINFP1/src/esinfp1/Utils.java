/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinfp1;

import Registo.RegistoReparticao;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import Listas.ListaServicos;
import Registo.RegistoReparticao.Node;

import java.util.List;

/**
 *
 * @author Carlos
 */
public class Utils {
   
    
    public static void main(String[] args) throws FileNotFoundException {
        lerFicheiroCidadao();
        lerFicheiroReparticao();
        lerFicheiroSenhas();
    }

    public static void lerFicheiroReparticao() throws FileNotFoundException {
        
        Scanner in = new Scanner(new File("fx_reparticoes.txt"));
        
        RegistoReparticao<Object> reparticoesDL = new RegistoReparticao<>();
        
        String[] linha;

        while (in.hasNext()) {
            List<Servico> listaServicos = new ArrayList<>();
              List<Cidadao> registoCidadao = new ArrayList<>();
            Reparticao r = new Reparticao(listaServicos,registoCidadao);
            linha = in.nextLine().trim().split(",");
            

            r.setCidade(linha[0]);
            r.setNreparticao(linha[1]);
            r.setPostCod(linha[2]);
            for (int i = 3; i < linha.length; i++) {
                Servico s = new Servico();
                s.setTipoServico(linha[i]);
                listaServicos.add(s);

            }
            r.setListaServicos(listaServicos);
            
            reparticoesDL.addReparticaoLast(r);
        }
    }

    public static void lerFicheiroCidadao() throws FileNotFoundException {
        Scanner in = new Scanner(new File("fx_cidadaos.txt"));
        List<Cidadao> registoCidadao = new ArrayList<>();
        String[] linha;

        while (in.hasNextLine()) {
            Cidadao cid = new Cidadao();
            linha = in.nextLine().trim().split(",");
            cid.setNome(linha[0]);
            cid.setNcontribuinte(linha[1]);
            cid.setEnderecoEmail(linha[2]);
            cid.setPostCod(linha[3]);
            cid.setNreparticao(linha[4]);
            registoCidadao.add(cid);
        }

        in.close();

    }

    public static void lerFicheiroSenhas() throws FileNotFoundException {
        Scanner in = new Scanner(new File("fx_senhas.txt"));
        String[] linha;
        Senha senha = new Senha();

        while (in.hasNext()) {
            linha = in.nextLine().trim().split(",");
            senha.setNContribuinte(linha[0]);
            senha.setCodAssunto(linha[1]);
            senha.setNordem(linha[2]);
        }
        
    }
        
}
