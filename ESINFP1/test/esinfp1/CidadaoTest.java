/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinfp1;

import Listas.ListaServicos;
import java.lang.reflect.Array;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tiago Ferreira
 */
public class CidadaoTest {
    
    public CidadaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNcontribuinte method, of class Cidadao.
     */
    @Test
    public void testGetNcontribuinte() {
        System.out.println("getNcontribuinte");
        Cidadao instance = new Cidadao("123456789", "nome", "email", "4200-460", "1234");
        String expResult = "123456789";
        String result = instance.getNcontribuinte();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNcontribuinte method, of class Cidadao.
     */
    @Test
    public void testSetNcontribuinte() {
        System.out.println("setNcontribuinte");
        String Ncontribuinte = "123456789";
        Cidadao instance = new Cidadao("123456789", "nome", "email", "4200-460", "1234");
        instance.setNcontribuinte(Ncontribuinte);
      
    }

    /**
     * Test of getNome method, of class Cidadao.
     */
    @Test
    public void testGetNome() {
        System.out.println("getNome");
        Cidadao instance = new Cidadao("123456789", "Tiago", "email", "4200-460", "1234");
        String expResult = "Tiago";
        String result = instance.getNome();
        assertEquals(expResult, result);
      
    }

    /**
     * Test of setNome method, of class Cidadao.
     */
    @Test
    public void testSetNome() {
        System.out.println("setNome");
        String Nome = "Tiago";
        Cidadao instance = new Cidadao("123456789", "nome", "email", "4200-460", "1234");
        instance.setNome(Nome);
    }

    /**
     * Test of getEnderecoEmail method, of class Cidadao.
     */
    @Test
    public void testGetEnderecoEmail() {
        System.out.println("getEnderecoEmail");
        Cidadao instance = new Cidadao("123456789", "nome", "email@isep.ipp.pt", "4200-460", "1234");
        String expResult = "email@isep.ipp.pt";
        String result = instance.getEnderecoEmail();
        assertEquals(expResult, result);
      
    }

    /**
     * Test of setEnderecoEmail method, of class Cidadao.
     */
    @Test
    public void testSetEnderecoEmail() {
        System.out.println("setEnderecoEmail");
        String EnderecoEmail = "email@isep.ipp.pt";
        Cidadao instance = new Cidadao("123456789", "nome", "email", "4200-460", "1234");
        instance.setEnderecoEmail(EnderecoEmail);
    }

    /**
     * Test of getPostCod method, of class Cidadao.
     */
    @Test
    public void testGetPostCod() {
        System.out.println("getPostCod");
        Cidadao instance = new Cidadao("123456789", "nome", "email", "4200-460", "1234");
        String expResult = "4200-460";
        String result = instance.getPostCod();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getPostCodReduced method, of class Cidadao.
     */
    @Test
    public void testGetPostCodReduced() {
        System.out.println("getPostCodReduced");
        Cidadao instance = new Cidadao("123456789", "nome", "email", "4200-460", "1234");
        String expResult = "4200";
        String result = instance.getPostCodReduced();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setPostCod method, of class Cidadao.
     */
    @Test
    public void testSetPostCod() {
        System.out.println("setPostCod");
        String PostCod = "4200-460";
        Cidadao instance = new Cidadao("123456789", "nome", "email", "4200-460", "1234");
        instance.setPostCod(PostCod);
       
    }

    /**
     * Test of getNreparticao method, of class Cidadao.
     */
    @Test
    public void testGetNreparticao() {
        System.out.println("getNreparticao");
        Cidadao instance = new Cidadao("123456789", "nome", "email", "4200-460", "1234");
        String expResult = "1234";
        String result = instance.getNreparticao();
        assertEquals(expResult, result);
     
    }

    /**
     * Test of setNreparticao method, of class Cidadao.
     */
    @Test
    public void testSetNreparticao() {
        System.out.println("setNreparticao");
        String Nreparticao = "1234";
        Cidadao instance = new Cidadao("123456789", "nome", "email", "4200-460", "1234");
        instance.setNreparticao(Nreparticao);
    }

  

    /**
     * Test of equals method, of class Cidadao.
     */
    @Test
    public void testEqualsFalse() {
        System.out.println("equalsFalse");
        Object obj = new Cidadao("123452789", "nome", "email", "4200-460", "1234");;
        Cidadao instance = new Cidadao("123456789", "nome", "email", "4200-460", "1234");
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
     public void testEqualsTrue() {
        System.out.println("equalsTrue");
        Object obj = new Cidadao("123456789", "nome", "email", "4200-460", "1234");;
        Cidadao instance = new Cidadao("123456789", "nome", "email", "4200-460", "1234");
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
       public void testEqualsClass() {
        System.out.println("equalsTrue");
        Servico serv =new Servico("A");
        List<Servico> listaServicos = new ArrayList<>();
        List<Cidadao> registoCidadao = new ArrayList<>();
        listaServicos.add(serv);
        Object obj = new Reparticao("Porto", "1234", "4785-054",listaServicos,registoCidadao);
        Cidadao instance = new Cidadao("123456789", "nome", "email", "4200-460", "1234");
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
}
