/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinfp1;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tiago Ferreira
 */
public class ReparticaoTest {

    public ReparticaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getCidade method, of class Reparticao.
     */
    @Test
    public void testGetCidade() {
        System.out.println("getCidade");
        Servico serv = new Servico("A");
        List<Servico> listaServicos = new ArrayList<>();
        List<Cidadao> registoCidadao = new ArrayList<>();
        listaServicos.add(serv);
        Reparticao instance = new Reparticao("Trofa", "1234", "4200-456", listaServicos, registoCidadao);
        String expResult = "Trofa";
        String result = instance.getCidade();
        assertEquals(expResult, result);

    }

    /**
     * Test of setCidade method, of class Reparticao.
     */
    @Test
    public void testSetCidade() {
        System.out.println("setCidade");
        Servico serv = new Servico("A");
        List<Servico> listaServicos = new ArrayList<>();
        List<Cidadao> registoCidadao = new ArrayList<>();
        listaServicos.add(serv);
        String cidade = "Trofa";
        Reparticao instance = new Reparticao("Trofa", "1234", "4200-456", listaServicos, registoCidadao);
        instance.setCidade(cidade);
    }

    /**
     * Test of getNreparticao method, of class Reparticao.
     */
    @Test
    public void testGetNreparticao() {
        System.out.println("getNreparticao");
        Servico serv = new Servico("A");
        List<Servico> listaServicos = new ArrayList<>();
        List<Cidadao> registoCidadao = new ArrayList<>();
        listaServicos.add(serv);
        Reparticao instance = new Reparticao("Trofa", "1234", "4200-456", listaServicos, registoCidadao);
        String expResult = "1234";
        String result = instance.getNreparticao();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNreparticao method, of class Reparticao.
     */
    @Test
    public void testSetNreparticao() {

        System.out.println("setNreparticao");
        Servico serv = new Servico("A");
        List<Servico> listaServicos = new ArrayList<>();
        List<Cidadao> registoCidadao = new ArrayList<>();
        listaServicos.add(serv);
        String Nreparticao = "1234";
        Reparticao instance = new Reparticao("Trofa", "1234", "4200-456", listaServicos, registoCidadao);
        instance.setNreparticao(Nreparticao);
    }

    /**
     * Test of getPostCod method, of class Reparticao.
     */
    @Test
    public void testGetPostCod() {
        System.out.println("getPostCod");
        Servico serv = new Servico("A");
        List<Servico> listaServicos = new ArrayList<>();
        List<Cidadao> registoCidadao = new ArrayList<>();
        listaServicos.add(serv);
        Reparticao instance = new Reparticao("Trofa", "1234", "4200-456", listaServicos, registoCidadao);
        String expResult = "4200-456";
        String result = instance.getPostCod();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPostCod method, of class Reparticao.
     */
    @Test
    public void testSetPostCod() {
        System.out.println("setPostCod");
        String postCod = "4200-456";
        Servico serv = new Servico("A");
        List<Servico> listaServicos = new ArrayList<>();
        List<Cidadao> registoCidadao = new ArrayList<>();
        listaServicos.add(serv);
        Reparticao instance = new Reparticao("Trofa", "1234", "4200-456", listaServicos, registoCidadao);
        instance.setPostCod(postCod);
    }

    /**
     * Test of getListaServicos method, of class Reparticao.
     */
    @Test
    public void testGetListaServicos() {
        System.out.println("getListaServicos");
        Servico serv = new Servico("A");
        List<Servico> listaServicos = new ArrayList<>();
        List<Cidadao> registoCidadao = new ArrayList<>();
        listaServicos.add(serv);
        Reparticao instance = new Reparticao("Trofa", "1234", "4200-456", listaServicos, registoCidadao);
        List<Servico> expResult = listaServicos;
        List<Servico> result = instance.getListaServicos();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaServicos method, of class Reparticao.
     */
    @Test
    public void testSetListaServicos() {
        System.out.println("setListaServicos");
        Servico serv = new Servico("A");
        List<Servico> listaServicos = new ArrayList<>();
        List<Cidadao> registoCidadao = new ArrayList<>();
        listaServicos.add(serv);
        Reparticao instance = new Reparticao("Trofa", "1234", "4200-456", listaServicos, registoCidadao);
        instance.setListaServicos(listaServicos);
    }

    /**
     * Test of equals method, of class Reparticao.
     */
    @Test
    public void testEqualsFalse() {
        System.out.println("equalsFalse");
        Servico serv = new Servico("A");
        List<Servico> listaServicos = new ArrayList<>();
        List<Cidadao> registoCidadao = new ArrayList<>();
        listaServicos.add(serv);
        Object obj = new Reparticao("Trofa", "1224", "4210-456", listaServicos, registoCidadao);
        Reparticao instance = new Reparticao("Troa", "1254", "4200-456", listaServicos, registoCidadao);
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    public void testEqualsTrue() {
        System.out.println("equalsTrue");
        Servico serv = new Servico("A");
        List<Servico> listaServicos = new ArrayList<>();
        List<Cidadao> registoCidadao = new ArrayList<>();
        listaServicos.add(serv);
        Object obj = new Reparticao("Trofa", "1234", "4200-456", listaServicos, registoCidadao);
        Reparticao instance = new Reparticao("Trofa", "1234", "4200-456", listaServicos, registoCidadao);
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    public void testEqualsClass() {
        System.out.println("equalsTrue");
        Servico serv = new Servico("A");
        List<Servico> listaServicos = new ArrayList<>();
        List<Cidadao> registoCidadao = new ArrayList<>();
        listaServicos.add(serv);
        Object obj = new Cidadao("123456789", "nome", "email", "4200-460", "1234");
        Reparticao instance = new Reparticao("Trofa", "1234", "4777-045", listaServicos, registoCidadao);
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

}
