/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinfp1;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tiago Ferreira
 */
public class SenhaTest {
    
    public SenhaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNordem method, of class Senha.
     */
    @Test
    public void testGetNordem() {
        System.out.println("getNordem");
        Senha instance = new Senha("1234", "A", "123456789");
        String expResult = "1234";
        String result = instance.getNordem();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNordem method, of class Senha.
     */
    @Test
    public void testSetNordem() {
        System.out.println("setNordem");
        String Nordem = "1234";
        Senha instance = new Senha("1234", "A", "123456789");
        instance.setNordem(Nordem);
    }

    /**
     * Test of getCodAssunto method, of class Senha.
     */
    @Test
    public void testGetCodAssunto() {
        System.out.println("getCodAssunto");
        Senha instance = new Senha("1234", "A", "123456789");
        String expResult = "A";
        String result = instance.getCodAssunto();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCodAssunto method, of class Senha.
     */
    @Test
    public void testSetCodAssunto() {
        System.out.println("setCodAssunto");
        String CodAssunto = "A";
        Senha instance = new Senha("1234", "A", "123456789");
        instance.setCodAssunto(CodAssunto);
    }

    /**
     * Test of getNContribuinte method, of class Senha.
     */
    @Test
    public void testGetNContribuinte() {
        System.out.println("getNContribuinte");
        Senha instance = new Senha("1234", "A", "123456789");
        String expResult = "123456789";
        String result = instance.getNContribuinte();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNContribuinte method, of class Senha.
     */
    @Test
    public void testSetNContribuinte() {
        System.out.println("setNContribuinte");
        String NContribuinte = "123456789";
        Senha instance = new Senha("1234", "A", "123456789");
        instance.setNContribuinte(NContribuinte);
    }

    /**
     * Test of hashCode method, of class Senha.
     */
   

    /**
     * Test of equals method, of class Senha.
     */
    @Test
 public void testEqualsFalse() {
        System.out.println("equalsFalse");
        Object obj = new Senha("1234", "A", "123456789");
        Senha instance = new Senha("1214", "B", "123453789");
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
     public void testEqualsTrue() {
        System.out.println("equalsTrue");
        Object obj = new Senha("1234", "A", "123456789");
        Senha instance = new Senha("1234", "A", "123456789");
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
       public void testEqualsClass() {
        System.out.println("equalsTrue");
        Servico serv =new Servico("A");
        List<Servico> listaServicos = new ArrayList<>();
        List<Cidadao> registoCidadao = new ArrayList<>();
        listaServicos.add(serv);
        Object obj = new Reparticao("Porto", "1234", "4785-054",listaServicos,registoCidadao);
        Senha instance = new Senha("1234", "A", "123456789");
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
}
