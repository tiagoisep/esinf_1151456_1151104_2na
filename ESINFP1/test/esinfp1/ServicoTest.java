/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinfp1;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tiago Ferreira
 */
public class ServicoTest {
    
    public ServicoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getTipoServico method, of class Servico.
     */
    @Test
    public void testGetTipoServico() {
        System.out.println("getTipoServico");
        Servico instance = new Servico("A");
        String expResult = "A";
        String result = instance.getTipoServico();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTipoServico method, of class Servico.
     */
    @Test
    public void testSetTipoServico() {
        System.out.println("setTipoServico");
        String tipoServico = "A";
        Servico instance = new Servico("A");
        instance.setTipoServico(tipoServico);
    }

  

  

    /**
     * Test of equals method, of class Servico.
     */
    @Test
    public void testEqualsFalse() {
        System.out.println("equalsFalse");
        Object obj = new Servico("A");
        Servico instance = new Servico("B");
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
     public void testEqualsTrue() {
        System.out.println("equalsTrue");
        Object obj = new Servico("A");;
        Servico instance = new Servico("A");
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
       public void testEqualsClass() {
        System.out.println("equalsTrue");
        Servico serv =new Servico("A");
        List<Servico> listaServicos = new ArrayList<>();
        List<Cidadao> registoCidadao = new ArrayList<>();
        listaServicos.add(serv);
        Object obj = new Reparticao("Porto", "1234", "4785-054",listaServicos,registoCidadao);
        Servico instance = new Servico("A");
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
}
